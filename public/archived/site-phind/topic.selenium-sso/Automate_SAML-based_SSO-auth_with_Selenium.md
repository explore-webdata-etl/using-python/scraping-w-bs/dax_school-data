---
created: 2024-02-25T13:56:32 (UTC -08:00)
tags: []
source: https://www.phind.com/search?cache=a98j50rla2gjnocny4gcfuj1
author: 
---

# Automate SAML-based SSO auth with Selenium

> ## Excerpt
> Get instant answers, explanations, and examples for all of your technical questions.

---
###### Answer | Phind-34B Model

Automating multi-factor authentication (MFA) with SAML using Selenium can be quite complex and challenging due to the security measures involved in MFA. MFA typically requires interaction with a second factor, such as a one-time password (OTP), a code sent to a mobile device, or a fingerprint scan, which cannot be easily automated using Selenium alone. However, for testing purposes or in controlled environments, you can automate the process by handling the second factor programmatically or by using third-party services to simulate MFA responses.

Below is a conceptual example of how you might approach automating MFA with SAML using Selenium. This example assumes you have a way to programmatically or manually handle the second factor. It's important to note that this approach should only be used in a secure, controlled environment, and never in a production setting.

### Example: Automating MFA with SAML using Selenium

This example is a simplified version and may need to be adjusted based on the specific MFA method and the SAML service provider you are working with.

#### Step 1: Automate the Initial Login

This step is similar to the standard SAML login process. You navigate to the login page, input your credentials, and submit the form.

```
<div><p><code id="code-lang-python"><span>from</span><span> selenium </span><span>import</span><span> webdriver
</span><span></span><span>from</span><span> selenium</span><span>.</span><span>webdriver</span><span>.</span><span>common</span><span>.</span><span>by </span><span>import</span><span> By
</span><span></span><span>from</span><span> selenium</span><span>.</span><span>webdriver</span><span>.</span><span>common</span><span>.</span><span>keys </span><span>import</span><span> Keys
</span><span></span><span>from</span><span> selenium</span><span>.</span><span>webdriver</span><span>.</span><span>support</span><span>.</span><span>ui </span><span>import</span><span> WebDriverWait
</span><span></span><span>from</span><span> selenium</span><span>.</span><span>webdriver</span><span>.</span><span>support </span><span>import</span><span> expected_conditions </span><span>as</span><span> EC
</span>
<span>driver </span><span>=</span><span> webdriver</span><span>.</span><span>Chrome</span><span>(</span><span>)</span><span>
</span>
<span></span><span># Navigate to the login page</span><span>
</span><span>driver</span><span>.</span><span>get</span><span>(</span><span>"https://your-sso-url.com/login"</span><span>)</span><span>
</span>
<span></span><span># Input username and password</span><span>
</span><span>username_input </span><span>=</span><span> WebDriverWait</span><span>(</span><span>driver</span><span>,</span><span>  </span><span>10</span><span>)</span><span>.</span><span>until</span><span>(</span><span>
</span><span>    EC</span><span>.</span><span>presence_of_element_located</span><span>(</span><span>(</span><span>By</span><span>.</span><span>ID</span><span>,</span><span> </span><span>"username"</span><span>)</span><span>)</span><span>
</span><span></span><span>)</span><span>
</span><span>username_input</span><span>.</span><span>send_keys</span><span>(</span><span>"your_username"</span><span>)</span><span>
</span>
<span>password_input </span><span>=</span><span> WebDriverWait</span><span>(</span><span>driver</span><span>,</span><span>  </span><span>10</span><span>)</span><span>.</span><span>until</span><span>(</span><span>
</span><span>    EC</span><span>.</span><span>presence_of_element_located</span><span>(</span><span>(</span><span>By</span><span>.</span><span>ID</span><span>,</span><span> </span><span>"password"</span><span>)</span><span>)</span><span>
</span><span></span><span>)</span><span>
</span><span>password_input</span><span>.</span><span>send_keys</span><span>(</span><span>"your_password"</span><span>)</span><span>
</span>
<span></span><span># Submit the form</span><span>
</span><span>password_input</span><span>.</span><span>send_keys</span><span>(</span><span>Keys</span><span>.</span><span>RETURN</span><span>)</span><span>
</span></code></p></div>
```

#### Step 2: Handle the Second Factor

This is where things get tricky. Depending on the type of MFA, you might need to:

-   **Manually input the second factor**: If you're automating this in a controlled environment, you might manually input the second factor after the script navigates to the MFA page.
-   **Use a third-party service**: Some MFA providers offer APIs that can simulate MFA responses for testing purposes. You would use these APIs to programmatically handle the second factor.

```
<div><p><code id="code-lang-python"><span># Example of waiting for the MFA page to load and then manually inputting the second factor</span><span>
</span><span>mfa_input </span><span>=</span><span> WebDriverWait</span><span>(</span><span>driver</span><span>,</span><span>  </span><span>10</span><span>)</span><span>.</span><span>until</span><span>(</span><span>
</span><span>    EC</span><span>.</span><span>presence_of_element_located</span><span>(</span><span>(</span><span>By</span><span>.</span><span>ID</span><span>,</span><span> </span><span>"mfa_code"</span><span>)</span><span>)</span><span>
</span><span></span><span>)</span><span>
</span>
<span></span><span># Manually input the MFA code here</span><span>
</span><span>mfa_input</span><span>.</span><span>send_keys</span><span>(</span><span>"123456"</span><span>)</span><span>
</span>
<span></span><span># Submit the MFA form</span><span>
</span><span>mfa_input</span><span>.</span><span>send_keys</span><span>(</span><span>Keys</span><span>.</span><span>RETURN</span><span>)</span><span>
</span></code></p></div>
```

### Important Considerations

-   **Security**: Automating MFA can pose significant security risks. Ensure that this automation is only used in a secure, controlled environment.
-   **Compliance**: Automating MFA may violate the terms of service of your SAML service provider or MFA provider. Always check the relevant policies and guidelines.
-   **Testing**: Use this approach responsibly for testing purposes only. Automated testing of MFA should not be used in production environments.

This example provides a basic framework for automating MFA with SAML using Selenium. The specific implementation details will vary depending on the MFA method and the SAML service provider. Always ensure that any automation you develop complies with relevant security policies and best practices.
