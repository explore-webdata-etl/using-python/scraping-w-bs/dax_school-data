---
created: 2024-02-16T15:54:03 (UTC -08:00)
tags: []
source: https://medium.com/@CodeYogi/how-to-scrape-websites-behind-a-login-with-python-b8e4efa9f5dd
author: Shane Lee
---

# How to Scrape Websites Behind a Login with Python | by Shane Lee | Medium

> ## Excerpt
> In this tutorial, we are going to cover web scrapping a website which has a login. So we are going to use the python module requests to login into the website and Beautiful Soup 4 to parse the HTML…

---
In this tutorial, we are going to cover web scrapping a website which has a login. So we are going to use the python module requests to login into the website and Beautiful Soup 4 to parse the HTML.

The first thing to do is inspect the website that we want to login to and scrape. The example website I will be using in this tutorial is [https://shorttracker.co.uk/](https://shorttracker.co.uk/)

Find the login form and then open the chrome developer tools.

Click on the network tab, and make sure you click ‘preserve log’. This, as you would expect, will preserve the network log, which is important because logging to a website is almost always redirected after the user is authenticated, meaning that the network log will be loss on the load of the new page, that is, unless we check the preserve log checkbox.

![](How%20to%20Scrape%20Websites%20Behind%20a%20Login%20with%20Python%20%20by%20Shane%20Lee%20%20Medium/0pQjGtXCNu9Dpt3Nj.jpg)

Then login in to the website.

Once you’ve logged in, look in the requests in the network tab for something that looks like ‘login’. The actual name of this will vary based on the website you’re scraping.

![](How%20to%20Scrape%20Websites%20Behind%20a%20Login%20with%20Python%20%20by%20Shane%20Lee%20%20Medium/0-0tes4btgoERX0Y2.png)

The request method is going to be POST. The status code is either going to be a 200 status code or in the 300s. 302 just means that the website redirects the user after they login.

Make a note of the request URL, we are going to need that.

Inspect the headers.

You’re going to want to include the ‘User-agent’ headers in your code.

```
'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'
```

There may be other headers which you need to include in the request, but it’s difficult to tell you what they will be, because each website different. See the code at the bottom of this page to see what else I included in the headers for this particular website.

Scroll down to the bottom of the login route information. You will see something like ‘Form Data’. You’ll know it’s what you’re looking for because it will have your username and your password in there. Something like this:

![](How%20to%20Scrape%20Websites%20Behind%20a%20Login%20with%20Python%20%20by%20Shane%20Lee%20%20Medium/0id4zhgbSpqcwoJTH.jpg)

Notice the csrfmiddlewaretoken. That’s used to prevent cross-site attacks. That’s something we are going to need, but we’re not going to copy and paste the actual token. We just need the key ‘csrfmiddlewaretoken’.

Make a note of all the keys in this form, (except ‘next’ we don’t need it for this tutorial). Make sure that you get the keys exactly right, if you don’t you won’t be able to login.

Let’s get down to some code!

First let’s import the modules we are going to use and define some constants: urls and headers

```
from bs4 import BeautifulSoup as bs
import requestsURL = 'https://shorttracker.co.uk/'
LOGIN_ROUTE = 'accounts/login/'HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'origin': URL, 'referer': URL + LOGIN_ROUTE}
```

Easy peasy.

Next, we are going to want to create a sessions object. This sessions object is going to hold all the information we need to persist the login when we visit different pages. Normally, when you browse a website that you’ve logged into via a web browser, you don’t have log in each time you visit a new page, this is because the browser retains your session data. This is what we are going to do with the sessions object. Every request we make will be made from this object.

```
s = requests.session()
```

In the example website used in this tutorial, we have to get hold of the csrf token, if we don’t, our requests are going to start getting blocked by the server.

So to do that, all we need to do is make a standard get request to the website and take the token we receive from the response to that request. Then we can use it in all subsequent responses.

Note you might not have to do this step, as not all websites implement the use of csrf tokens. You’ll know if you do, because you’ll see it in the form data when you login in the above network requests.

So a simple get request and then pull the csrftoken from the cookies of that request:

```
csrf_token = s.get(URL).cookies['csrftoken']
```

Wunderbar!

Now, we just need to construct our login payload that we are going to post to the login route of the website we are going to scrape. Again make sure that the keys you are using here match the keys you saw in the form data object exactly, otherwise you won’t be able to login.

```
login_payload = {
        'login': <yourloginhere>,
        'password': <yourpasswordhere>, 
        'csrfmiddlewaretoken': csrf_token
        }
```

Now we’re ready to login.

We’re just going to create a simple POST request which goes to the login route, includes the headers we defined earlier, and of course includes our login payload.

```
login_req = s.post(URL + LOGIN_ROUTE, 
                   headers=HEADERS, 
                   data=login_payload)
```

Now, we should be logged in. We can verify this by printing the status code of the request. If we have logged in, it should be 200.

```
print(login_req.status_code)
```

I’ve noticed that sometimes the requests session object doesn’t always retain the cookies that it should. So to make sure that we keep them let’s save them and include them in any subsequent requests.

```
cookies = login_req.cookies
```

Now you’re all logged and can scrape to your hearts content.

I’m just going to use Beautiful Soup to get a table from the watchlist page of the example website, just to show that this example works.

```
soup = bs(s.get(URL + 'watchlist').text, 'html.parser')
tbody = soup.find('table', id='companies')
print(tbody)
```

And… we’re done. At least for this tutorial. Hopefully, you have found this useful.

Here is the complete code for this tutorial:

```
from bs4 import BeautifulSoup as bs
import requestsURL = 'https://shorttracker.co.uk/'
LOGIN_ROUTE = 'accounts/login/'HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'origin': URL, 'referer': URL + LOGIN_ROUTE}s = requests.session()csrf_token = s.get(URL).cookies['csrftoken']login_payload = {
        'login': <yourloginhere>,
        'password': <yourpasswordhere>, 
        'csrfmiddlewaretoken': csrf_token
        }login_req = s.post(URL + LOGIN_ROUTE, headers=HEADERS, data=login_payload)print(login_req.status_code)cookies = login_req.cookiessoup = bs(s.get(URL + 'watchlist').text, 'html.parser')
tbody = soup.find('table', id='companies')
print(tbody)
```

Here is the video that accompanies this tutorial: [https://www.youtube.com/watch?v=SA18JCBtlXY](https://www.youtube.com/watch?v=SA18JCBtlXY)

If you have any questions or have any feedback (definitely welcome!), then please leave a comment below, or you can find me at:

-   [http://youtube.com/shaneLeeCoding](http://youtube.com/shaneLeeCoding)
-   [https://github.com/ShaneLee](https://github.com/ShaneLee)
-   [https://codeyogi.co.uk/](https://codeyogi.co.uk/)

Article originally posted at: [https://codeyogi.co.uk/2020/01/09/how-to-login-and-scrape-websites-with-python/(opens in a new tab)](https://codeyogi.co.uk/?p=493)
