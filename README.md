# dax_school-data: Data Acquisition for School Data

### In DynaList:: [project (GL): [dax_school-data]](https://dynalist.io/d/UiPIpMMAyzCtJMhZHRXNKxw2#z=ZNdfcA1vZHPUR5pVr4DBiove)

---

### Running the project (in its Jupyter notebook):
You can run the project with the following steps:
1. `cd ~/.../project.directory`
1. `source ./project_venv/bin/activate` 
    * Assuming you have previously run `./setup.project.venv.bash` (which is executable)
1. `jupyter notebook daq_school-data &` 
    * Which you can bring back to foreground with the standard Unix `fg` and `CTRL-d` cycle...`
    * If you are running multiple notebook instances, you can use a different port:
      *  `jupyter notebook my-notebook.ipynb --port=8081`    
      *  `jupyter notebook my-notebook.ipynb --ip=0.0.0.0 --port=8081`

---

### Our [Project Wiki](https://gitlab.com/explore-webdata-etl/using-python/scraping-w-bs/dax_school-data/-/wikis/home)

Here you will find: 
* Useful Resources, 
* Tutorials, 
* & Benchmark Projects

...hopefully protected against link-rot in our cute lil-ole local wiki.


